import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import HomeSection from "./Sections/HomeSection";
import RegisterUser from "./Sections/RegisterUser";
import ComputerSurvey from "./Sections/ComputerSurvey";
import AddPartSection from "./Sections/AddPartSection";
import AddCpuForm from "./Sections/AddPartForms/AddCpuForm";
import AddRamForm from "./Sections/AddPartForms/AddRamForm";
import AddCaseForm from "./Sections/AddPartForms/AddCaseForm";
import AddVideoCardForm from "./Sections/AddPartForms/AddVideoCardForm";
import AddPowerSupplyForm from "./Sections/AddPartForms/AddPowerSupplyForm";
import AddHardDiskForm from "./Sections/AddPartForms/AddHardDiskForm";
import AddMotherboardForm from "./Sections/AddPartForms/AddMotherboardForm";
import AddCpuCoolerForm from "./Sections/AddPartForms/AddCpuCoolerForm";
import UserAdministration from "./Sections/UserAdministration";
import PartLists from "./Sections/PartLists";
import RamList from "./Sections/PartLists/RamList";
import CaseList from "./Sections/PartLists/CaseList";
import CpuList from "./Sections/PartLists/CpuList";
import HardDiskList from "./Sections/PartLists/HardDiskList";
import VideoCardList from "./Sections/PartLists/VideoCardList";
import CPUCoolerList from "./Sections/PartLists/CpuCoolerList";
import PowerSupplyList from "./Sections/PartLists/PowerSupplyList";
import MotherboardList from "./Sections/PartLists/MotherboardList";
import SurveyResults from "./Sections/SurveyResults";
import ListCreation from "./Sections/ListCreation";

import './App.css';


class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route exact path="/" component={HomeSection}/>
                        <Route exact path="/Registro" component={RegisterUser}/>
                        <Route exact path="/Survey" component={ComputerSurvey}/>
                        <Route exact path="/Survey/:gaming/:video/:oficina/:rendering" component={SurveyResults}/>
                        <Route exact path="/AddPart" component={AddPartSection}/>
                        <Route exact path="/AddPart/AddCpu" component={AddCpuForm}/>
                        <Route exact path="/AddPart/AddRam" component={AddRamForm}/>
                        <Route exact path="/AddPart/AddCase" component={AddCaseForm}/>
                        <Route exact path="/AddPart/AddVideoCard" component={AddVideoCardForm}/>
                        <Route exact path="/AddPart/AddPowerSupply" component={AddPowerSupplyForm}/>
                        <Route exact path="/AddPart/AddHardDisk" component={AddHardDiskForm}/>
                        <Route exact path="/AddPart/AddMotherboard" component={AddMotherboardForm}/>
                        <Route exact path="/AddPart/AddCpuCooler" component={AddCpuCoolerForm}/>
                        <Route exact path="/UserAdministration" component={UserAdministration}/>
                        <Route exact path="/PartLists" component={PartLists}/>
                        <Route exact path="/PartLists/RamList" component={RamList}/>
                        <Route exact path="/PartLists/CaseList" component={CaseList}/>
                        <Route exact path="/PartLists/CpuList" component={CpuList}/>
                        <Route exact path="/PartLists/HardDiskList" component={HardDiskList}/>
                        <Route exact path="/PartLists/VideoCardList" component={VideoCardList}/>
                        <Route exact path="/PartLists/CpuCoolerList" component={CPUCoolerList} />
                        <Route exact path="/PartLists/PowerSupplyList" component={PowerSupplyList}/>
                        <Route exact path="/PartLists/MotherboardList" component={MotherboardList}/>
                        <Route exact path="/ListCreation" component={ListCreation}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
