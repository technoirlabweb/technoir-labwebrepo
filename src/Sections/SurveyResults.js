import React, {Component} from 'react';
import {Layout} from 'antd';
import ComputerCard from '../Components/ComputerCard';
import HeaderAdministrator from '../Components/HeaderAdministrator'

const {Footer, Content} = Layout;

class SurveyResults extends Component {

    constructor(props) {
        super(props);
        this.state = {computers: []};
    }

    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>¡Aquí tus resultados!</h1>
                            <ComputerCard image="https://www.shutterstock.com/es/image-vector/help-me-vector-banner-on-white-608978792" title="Encontrar la PC para mi"
                                          description="Para usuarios principiantes" link="/Survey"/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default SurveyResults