import React, {Component} from 'react';
import {Layout, Avatar} from 'antd';
import {Row, Col, List} from 'antd';
import {Link} from 'react-router-dom'
import cpuImage from '../Media/AddPartSection/cpu.jpg'
import ramImage from '../Media/AddPartSection/ram.png'
import videoCardImage from '../Media/AddPartSection/videoCard.jpg'
import powerSupplyImage from '../Media/AddPartSection/powerSupply.jpg'
import caseImage from '../Media/AddPartSection/case.jpg'
import cpuCoolerImage from '../Media/AddPartSection/cpuCooler.jpg'
import motherboardImage from '../Media/AddPartSection/motherboard.jpg'
import hardDiskImage from '../Media/AddPartSection/hardDisk.jpg'
import HeaderAdministrator from '../Components/HeaderAdministrator'


const {Footer, Content} = Layout;

const data = [
    {
        title: 'Añadir un CPU',
        link: "/AddPart/AddCpu",
        avatar: cpuImage,
    },
    {
        title: 'Añadir un módulo de memoria RAM',
        link: "/AddPart/AddRam",
        avatar: ramImage,
    },
    {
        title: 'Añadir una tarjeta de video',
        link: "/AddPart/AddVideoCard",
        avatar: videoCardImage,
    },
    {
        title: 'Añadir una fuente de poder',
        link: "/AddPart/AddPowerSupply",
        avatar: powerSupplyImage,
    },
    {
        title: 'Añadir un gabinete',
        link: "/AddPart/AddCase",
        avatar: caseImage,
    },
    {
        title: 'Añadir un refrigerador de CPU',
        link: "/AddPart/AddCpuCooler",
        avatar: cpuCoolerImage,
    },
    {
        title: 'Añadir una tarjeta madre',
        link: "/AddPart/AddMotherboard",
        avatar: motherboardImage,
    },
    {
        title: 'Añadir un disco duro',
        link: "/AddPart/AddHardDisk",
        avatar: hardDiskImage,
    }
];


class AddPartSection extends Component {

    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div>
                            <h1 className="title" style={{textAlign: 'center'}}>Añadir una pieza</h1>
                            <Row type="flex" justify="center">
                                <Col span={8}>
                                    <List
                                        itemLayout="horizontal"
                                        dataSource={data}
                                        renderItem={item => (
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={<Avatar
                                                        src={item.avatar}/>}
                                                    title={<Link to={item.link}>{item.title}</Link>}

                                                />
                                            </List.Item>
                                        )}
                                    />
                                </Col>
                            </Row>


                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );

    }
}

export default AddPartSection