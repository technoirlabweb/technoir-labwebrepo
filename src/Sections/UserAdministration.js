import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';
import HeaderAdministrator from '../Components/HeaderAdministrator'

const columns = [{
    title: 'Nombre',
    dataIndex: 'nombre',
    key: 'nombre',
}, {
    title: 'Username',
    dataIndex: 'username',
    key: 'username',
}, {
    title: 'Tipo',
    dataIndex: 'tipo',
    key: 'tipo',
}, {
    title: 'Correo',
    dataIndex: 'correo',
    key: 'correo',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];

const {Footer, Content} = Layout;


class UserAdministration extends Component {

    constructor(props) {
        super(props);
        this.state = {users: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/usuario/all")
            .then(res => {
                this.setState({users: res.data});
            })
    }


    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Administración de usuarios</h1>
                            <Table className="listTable" rowKey={record => record.id} columns={columns} dataSource={this.state.users}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default UserAdministration