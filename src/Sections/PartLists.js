import React, {Component} from 'react';
import {Layout, Avatar} from 'antd';
import {Row, Col, List} from 'antd';
import {Link} from 'react-router-dom'
import cpuImage from '../Media/AddPartSection/cpu.jpg'
import ramImage from '../Media/AddPartSection/ram.png'
import videoCardImage from '../Media/AddPartSection/videoCard.jpg'
import powerSupplyImage from '../Media/AddPartSection/powerSupply.jpg'
import caseImage from '../Media/AddPartSection/case.jpg'
import cpuCoolerImage from '../Media/AddPartSection/cpuCooler.jpg'
import motherboardImage from '../Media/AddPartSection/motherboard.jpg'
import hardDiskImage from '../Media/AddPartSection/hardDisk.jpg'
import HeaderAdministrator from '../Components/HeaderAdministrator'


const {Footer, Content} = Layout;

const data = [
    {
        title: 'Procesadores',
        link: "/PartLists/CpuList",
        avatar: cpuImage,
    },
    {
        title: 'Memorias RAM',
        link: "/PartLists/RamList",
        avatar: ramImage,
    },
    {
        title: 'Tarjetas de video',
        link: "/PartLists/VideoCardList",
        avatar: videoCardImage,
    },
    {
        title: 'Fuentes de poder',
        link: "/PartLists/PowerSupplyList",
        avatar: powerSupplyImage,
    },
    {
        title: 'Gabinetes',
        link: "/PartLists/CaseList",
        avatar: caseImage,
    },
    {
        title: 'Refrigeradores de CPU',
        link: "/PartLists/CpuCoolerList",
        avatar: cpuCoolerImage,
    },
    {
        title: 'Tarjetas Madre',
        link: "/PartLists/MotherboardList",
        avatar: motherboardImage,
    },
    {
        title: 'Discos duros',
        link: "/PartLists/HardDiskList",
        avatar: hardDiskImage,
    }
];


class AddPartSection extends Component {

    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div>
                            <h1 className="title" style={{textAlign: 'center'}}>Ver piezas</h1>
                            <Row type="flex" justify="center">
                                <Col span={8}>
                                    <List
                                        itemLayout="horizontal"
                                        dataSource={data}
                                        renderItem={item => (
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={<Avatar
                                                        src={item.avatar}/>}
                                                    title={<Link to={item.link}>{item.title}</Link>}

                                                />
                                            </List.Item>
                                        )}
                                    />
                                </Col>
                            </Row>


                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );

    }
}

export default AddPartSection