import React, {Component} from 'react';
import {Layout, Carousel} from 'antd';
import {Row, Col} from 'antd';
import imgPrincipiante from '../Media/principiantePC.jpg';
import imgAvanzado from '../Media/avanzadoPC.jpg';
import MenuCard from '../Components/HomeSection/MenuCard'
import HeaderAdministrator from '../Components/HeaderAdministrator'
import car1 from '../Media/Carousel/1.jpg'
import car2 from '../Media/Carousel/2.jpg'
import car3 from '../Media/Carousel/3.JPG'
import car4 from '../Media/Carousel/4.jpg'
import technoirHome from '../Media/technoirHome.png'


const {Footer, Content} = Layout;


class HomeSection extends Component {

    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div>
                            <div className="technoirHome"><img alt="Error" src={technoirHome}></img></div>
                            <Row className="mainDescription" type="flex" justify="center">
                                <Col span={24}>
                                    <h2>Tu computadora, fácil</h2>
                                    <div className="homeDescription">
                                        Sin importar tu conocimiento en computadoras, Technoir te facilita la
                                        oportunidad de conseguir la computadora de tus sueños
                                    </div>
                                    <Carousel autoplay>
                                        <div><img alt="Error" className="carImg" src={car1} ></img></div>
                                        <div><img alt="Error" className="carImg"src={car2}></img></div>
                                        <div><img alt="Error" className="carImg" src={car3}></img></div>
                                        <div><img alt="Error" className="carImg" src={car4}></img></div>
                                    </Carousel>
                                </Col>
                            </Row>
                            <Row type="flex" justify="center">
                                <Col span={12}>
                                    <MenuCard image={imgPrincipiante} title="Encontrar la PC para mi"
                                              description="Para usuarios principiantes" link="/Survey"/>
                                </Col>
                                <Col span={12}>
                                    <MenuCard image={imgAvanzado} title="Armar una PC desde cero"
                                              description="Para usuarios avanzados" link="/ListCreation"/>
                                </Col>
                            </Row>


                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );

    }
}

export default HomeSection