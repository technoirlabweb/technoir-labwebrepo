import React, {Component} from 'react';
import {Layout} from 'antd';
import {Form} from 'antd';
import SurveyForm from '../Components/ComputerSurvey/SurveyForm'
import HeaderAdministrator from '../Components/HeaderAdministrator'


const {Footer, Content} = Layout;
const WrappedRegistrationForm = Form.create()(SurveyForm);

class ComputerSurvey extends Component {


    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Encontrar la PC para mi</h1>
                            <div className="surveyDescription">
                                Responda las siguiente preguntas para determinar qué equipo es el mejor para usted
                            </div>
                            <WrappedRegistrationForm />
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default ComputerSurvey