import React, {Component} from 'react';
import {Layout, Card, Modal} from 'antd';
import CpuListMin from '../Components/ListCreation/CpuListMin';
import MotherboardListMin from '../Components/ListCreation/MotherboardListMin';
import RamListMin from '../Components/ListCreation/RamListMin';
import VideoCardListMin from '../Components/ListCreation/VideoCardListMin';
import PowerSupplyListMin from '../Components/ListCreation/PowerSupplyListMin';
import CaseListMin from '../Components/ListCreation/CaseListMin';
import HardDiskListMin from '../Components/ListCreation/HardDiskListMin';
import CpuCoolerListMin from '../Components/ListCreation/CpuCoolerListMin';
import HeaderAdministrator from '../Components/HeaderAdministrator'


const {Footer, Content} = Layout;


class ListCreation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modals: {
                modal1Visible: false,
                modal2Visible: false,
                modal3Visible: false,
                modal4Visible: false,
                modal5Visible: false,
                modal6Visible: false,
                modal7Visible: false,
                modal8Visible: false,
            }
            , computer: {
                cpu: {
                    "id": 0,
                    "uuid": "",
                    "manufacturer": "",
                    "model": "",
                    "socket": "",
                    "operating_frequency": "",
                    "na_max_frequency": "",
                    "na_cores": "",
                    "estatus": 1,
                    "fecha_creacion": "2018-05-09",
                    "fecha_modificacion": "2018-05-09"
                },

                cosas: {
                    "id": 0,
                    "uuid": "",
                    "manufacturer": "",
                    "part": "",
                    "type": "",
                    "speed": "",
                    "size": "",
                    "estatus": 1,
                    "fecha_creacion": "2018-05-09",
                    "fecha_modificacion": "2018-05-09"
                },
                gpu: {},
                hdd: {},
                cooler: {},
                case: {},
                psu: {}
            }, ram: {
                "id": 0,
                "uuid": "",
                "manufacturer": "",
                "part": "",
                "type": "",
                "speed": "",
                "size": "",
                "estatus": 1,
                "fecha_creacion": "2018-05-09",
                "fecha_modificacion": "2018-05-09"
            }, motherboard: {
                "id": 0,
                "uuid": "",
                "manufacturer": "",
                "part": "",
                "form_factor": "",
                "cpu_socket": "",
                "chipset": "",
                "memory_slots": 0,
                "crossfire_support": 0,
                "sli_support": 0,
                "estatus": 1,
                "fecha_creacion": "2018-05-09",
                "fecha_modificacion": "2018-05-09"
            }
        };

        this.addCpu = this.addCpu.bind(this);
        this.addRam = this.addRam.bind(this);
    }

    addCpu(_cpu) {
        var temp = this.state;
        temp.computer.cpu = _cpu;
        this.setState(temp);
    }

    addMotherboard(_motherboard) {
        this.setState({motherboard: _motherboard})
    }


    addRam(_ram) {
        var temp = this.state;
        temp.ram = _ram;
        this.setState(temp);

    }

    addGpu(_gpu) {
        this.setState({gpu: _gpu})
    }

    addHdd(_hdd) {
        this.setState({hdd: _hdd})
    }

    addCooler(_cooler) {
        this.setState({cooler: _cooler})
    }

    addCase(_case) {
        this.setState({case: _case})
    }

    addPsu(_psu) {
        this.setState({psu: _psu})
    }

    setModal1Visible(modal1Visible) {
        this.setState({modal1Visible});
    }

    setModal2Visible(modal2Visible) {
        this.setState({modal2Visible});
    }

    setModal3Visible(modal3Visible) {
        this.setState({modal3Visible});
    }

    setModal4Visible(modal4Visible) {
        this.setState({modal4Visible});
    }

    setModal5Visible(modal5Visible) {
        this.setState({modal5Visible});
    }

    setModal6Visible(modal6Visible) {
        this.setState({modal6Visible});
    }

    setModal7Visible(modal7Visible) {
        this.setState({modal7Visible});
    }

    setModal8Visible(modal8Visible) {
        this.setState({modal8Visible});
    }

    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div id="test">


                            <h1 className="title" style={{textAlign: 'center'}}>Crear una PC desde cero</h1>
                            <ul>
                                <li className="listElement">
                                    <Card className="selectionList" title="CPU"
                                          extra={<a onClick={() => this.setModal1Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                        <ul>
                                            <li>Fabricante: {this.state.computer.cpu.manufacturer}</li>
                                            <li>Socket: {this.state.computer.cpu.socket}</li>
                                            <li>Número de núcleos: {this.state.computer.cpu.na_cores}</li>
                                        </ul>
                                    </Card>
                                </li>


                                <Modal
                                    title="Lista de CPU's"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal1Visible}
                                    onOk={() => this.setModal1Visible(false)}
                                    onCancel={() => this.setModal1Visible(false)}
                                >
                                    <CpuListMin addCpu={this.addCpu}/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Tarjeta Madre"
                                          extra={<a onClick={() => this.setModal2Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                        <ul>

                                        </ul>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de tarjetas madre"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal2Visible}
                                    onOk={() => this.setModal2Visible(false)}
                                    onCancel={() => this.setModal2Visible(false)}
                                >
                                    <MotherboardListMin addMotherboard={this.addMotherboard}/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Memoria RAM"
                                          extra={<a onClick={() => this.setModal3Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                        <ul>
                                            <li>Fabricante: {this.state.ram.manufacturer}</li>
                                            <li>Velocidad: {this.state.ram.speed}</li>
                                            <li>Tipo {this.state.ram.type}</li>
                                        </ul>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de memorias RAM"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal3Visible}
                                    onOk={() => this.setModal3Visible(false)}
                                    onCancel={() => this.setModal3Visible(false)}
                                >
                                    <RamListMin addRam={this.addRam}/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Tarjeta de video"
                                          extra={<a onClick={() => this.setModal4Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de tarjetas de video"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal4Visible}
                                    onOk={() => this.setModal4Visible(false)}
                                    onCancel={() => this.setModal4Visible(false)}
                                >
                                    <VideoCardListMin/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Fuente de poder"
                                          extra={<a onClick={() => this.setModal5Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de fuentes de poder"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal5Visible}
                                    onOk={() => this.setModal5Visible(false)}
                                    onCancel={() => this.setModal5Visible(false)}
                                >
                                    <PowerSupplyListMin/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Gabinete"
                                          extra={<a onClick={() => this.setModal6Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de gabinetes"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal6Visible}
                                    onOk={() => this.setModal6Visible(false)}
                                    onCancel={() => this.setModal6Visible(false)}
                                >
                                    <CaseListMin/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Discos Duros"
                                          extra={<a onClick={() => this.setModal7Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de discos duros"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal7Visible}
                                    onOk={() => this.setModal7Visible(false)}
                                    onCancel={() => this.setModal7Visible(false)}
                                >
                                    <HardDiskListMin/>
                                </Modal>

                                <li className="listElement">
                                    <Card className="selectionList" title="Refrigerador de CPU"
                                          extra={<a onClick={() => this.setModal8Visible(true)}>Piezas</a>}
                                          style={{width: 300}}>
                                    </Card>
                                </li>

                                <Modal
                                    title="Lista de refrigeradores de CPU"
                                    style={{top: 20}}
                                    width={1300}
                                    wrapClassName="vertical-center-modal"
                                    visible={this.state.modal8Visible}
                                    onOk={() => this.setModal8Visible(false)}
                                    onCancel={() => this.setModal8Visible(false)}
                                >
                                    <CpuCoolerListMin/>
                                </Modal>
                            </ul>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default ListCreation