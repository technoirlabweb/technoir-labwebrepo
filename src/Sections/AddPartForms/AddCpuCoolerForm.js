import React, {Component} from 'react';
import {Layout} from 'antd';
import {Form} from 'antd';
import RegistrationForm from '../../Components/AddCpuCoolerForm/AddCpuCoolerSurvey'
import HeaderAdministrator from '../../Components/HeaderAdministrator';


const {Footer, Content} = Layout;
const WrappedRegistrationForm = Form.create()(RegistrationForm);

class AddCpuCoolerForm extends Component {



    render() {
        return (
            <div>
                <Layout>
                    <HeaderAdministrator/>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Añadir un refrigerador de CPU</h1>
                            <WrappedRegistrationForm />
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default AddCpuCoolerForm