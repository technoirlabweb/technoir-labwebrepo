import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Número de parte',
    dataIndex: 'part',
    key: 'part',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Color',
    dataIndex: 'color',
    key: 'color',
}, {
    title: 'Tipo',
    dataIndex: 'type',
    key: 'type',
}, {
    title: 'Fecha de creación',
    dataIndex: 'fecha_creacion',
    key: 'fecha_creacion',
}, {
    title: 'Fecha de modificación',
    dataIndex: 'fecha_modificacion',
    key: 'fecha_modificacion',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];


const {Header, Footer, Content} = Layout;


class CaseList extends Component {

    constructor(props) {
        super(props);
        this.state = {cases: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/case/all")
            .then(res => {
                this.setState({cases: res.data});
            })
    }

    render() {
        return (
            <div>
                <Layout>
                    <Header>Header</Header>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Lista de gabinetes</h1>
                            <Table className="listTable" rowKey={record => record.id} columns={columns}
                                   dataSource={this.state.cases}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default CaseList