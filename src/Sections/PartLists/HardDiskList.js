import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Número de parte',
    dataIndex: 'part',
    key: 'part',
}, {
    title: 'Capacidad',
    dataIndex: 'capacity',
    key: 'capacity',
}, {
    title: 'Factor de forma',
    dataIndex: 'form_factor',
    key: 'form_factor',
}, {
    title: 'Estado sólido',
    dataIndex: 'solid_state',
    key: 'solid_state',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Fecha de creación',
    dataIndex: 'fecha_creacion',
    key: 'fecha_creacion',
}, {
    title: 'Fecha de modificación',
    dataIndex: 'fecha_modificacion',
    key: 'fecha_modificacion',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];

const {Header, Footer, Content} = Layout;


class HardDiskList extends Component {

    constructor(props) {
        super(props);
        this.state = {hardDisks: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/hard_disk/all")
            .then(res => {
                this.setState({hardDisks: res.data});
            })
    }


    render() {
        return (
            <div>
                <Layout>
                    <Header>Header</Header>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Lista de discos duros</h1>
                            <Table rowKey={record => record.id} className="listTable" columns={columns}
                                   dataSource={this.state.hardDisks}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default HardDiskList