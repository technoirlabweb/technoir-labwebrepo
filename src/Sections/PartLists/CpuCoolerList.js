import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Nivel de ruido',
    dataIndex: 'noise_level',
    key: 'noise_level',
}, {
    title: 'Fan RPM',
    dataIndex: 'fan_rpm',
    key: 'fan_rpm',
}, {
    title: 'Refrigerador líquido',
    dataIndex: 'liquid_cooled',
    key: 'liquid_cooled',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Fecha de creación',
    dataIndex: 'fecha_creacion',
    key: 'fecha_creacion',
}, {
    title: 'Fecha de modificación',
    dataIndex: 'fecha_modificacion',
    key: 'fecha_modificacion',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];

const {Header, Footer, Content} = Layout;

class CpuCoolerList extends Component {

    constructor(props) {
        super(props);
        this.state = {cpuCoolers: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/cpu_cooler/all")
            .then(res => {
                this.setState({cpuCoolers: res.data});
            })
    }


    render() {
        return (
            <div>
                <Layout>
                    <Header>Header</Header>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Lista de Refrigeradores de CPU</h1>
                            <Table rowKey={record => record.id} className="listTable" columns={columns}
                                   dataSource={this.state.cpuCoolers}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default CpuCoolerList