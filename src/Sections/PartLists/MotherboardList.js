import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Número de parte',
    dataIndex: 'part',
    key: 'part',
}, {
    title: 'Factor de forma',
    dataIndex: 'form_factor',
    key: 'form_factor',
}, {
    title: 'Socket CPU',
    dataIndex: 'cpu_socket',
    key: 'cpu_socket',
}, {
    title: 'Chipset',
    dataIndex: 'chipset',
    key: 'chipset',
}, {
    title: 'Soporte de SLI',
    dataIndex: 'sli_support',
    key: 'sli_support',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Soporte de CrossFire',
    dataIndex: 'crossfire_support',
    key: 'crossfire_support',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Fecha de creación',
    dataIndex: 'fecha_creacion',
    key: 'fecha_creacion',
}, {
    title: 'Fecha de modificación',
    dataIndex: 'fecha_modificacion',
    key: 'fecha_modificacion',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];

const {Header, Footer, Content} = Layout;


class MotherboardList extends Component {

    constructor(props) {
        super(props);
        this.state = {motherboards: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/motherboard/all")
            .then(res => {
                this.setState({motherboards: res.data});
            })
    }


    render() {
        return (
            <div>
                <Layout>
                    <Header>Header</Header>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Lista de tarjetas madre</h1>
                            <Table rowKey={record => record.id} className="listTable" columns={columns}
                                   dataSource={this.state.motherboards}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default MotherboardList