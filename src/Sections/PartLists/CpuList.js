import React, {Component} from 'react';
import {Layout} from 'antd';
import {Table, Divider} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Socket',
    dataIndex: 'socket',
    key: 'socket',
}, {
    title: 'Frecuencia de operación (GHz)',
    dataIndex: 'operating_frequency',
    key: 'operating_frequency',
}, {
    title: 'Frecuencia máxima de operación (GHz)',
    dataIndex: 'na_max_frequency',
    key: 'na_max_frequency',
}, {
    title: 'Número de núcleos',
    dataIndex: 'na_cores',
    key: 'na_cores',
}, {
    title: 'Fecha creación',
    dataIndex: 'fecha_creacion',
    key: 'fecha_creacion',
}, {
    title: 'Fecha modificación',
    dataIndex: 'fecha_modificacion',
    key: 'fecha_modificacion',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Editar</a>
      <Divider type="vertical"/>
      <a href="">Eliminar</a>
    </span>
    ),
}];



const {Header, Footer, Content} = Layout;


class CpuList extends Component {

    constructor(props) {
        super(props);
        this.state = {cpus: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/cpu/all")
            .then(res => {
                this.setState({cpus: res.data});
            })
    }


    render() {
        return (
            <div>
                <Layout>
                    <Header>Header</Header>
                    <Content>
                        <div id="test">
                            <h1 className="title" style={{textAlign: 'center'}}>Lista de procesadores</h1>
                            <Table rowKey={record => record.id} className="listTable" columns={columns}
                                   dataSource={this.state.cpus}/>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        Technoir ©2018
                    </Footer>
                </Layout>
            </div>
        );
    }
}

export default CpuList