import React from 'react'
import {Card} from 'antd';
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';

const {Meta} = Card;


const MenuCard = ({image, title, description, link}) =>
    <Link to={link}>
        <Card className="menuCard"
              hoverable
              style={{width: '50%'}}
              cover={<img alt=""
                          src={image}/>}
        >
            <Meta
                title={title}
                description={description}
            />
        </Card>
    </Link>;

MenuCard.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    link: PropTypes.string
}

MenuCard.defaultProps = {
    image: "Default",
    title: "Armado de PC's",
    description: "Tutorial Básico",
    link: "/"
}

export default MenuCard

