import React from 'react';
import {Form, Input, Tooltip, Icon, Button} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;

const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};

class RegistrationForm extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'localhost:8080/v1/user',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                        console.log(values);
                    });
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Las contraseñas no coinciden');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerForm" onSubmit={this.handleSubmit}>
                <FormItem
                    {...formItemLayout}
                    label="E-mail"
                >
                    {getFieldDecorator('correo', {
                        rules: [{
                            type: 'email', message: 'El correo ingresado no es válido',
                        }, {
                            required: true, message: 'Por favor introduzca su correo',
                        }],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Contraseña"
                >
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true, message: 'Porfavor introduzca su contraseña',
                        }, {
                            validator: this.validateToNextPassword,
                        }],
                    })(
                        <Input type="password"/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Confirmar contraseña"
                >
                    {getFieldDecorator('confirm', {
                        rules: [{
                            required: true, message: 'Por favor confirme su contraseña',
                        }, {
                            validator: this.compareToFirstPassword,
                        }],
                    })(
                        <Input type="password" onBlur={this.handleConfirmBlur}/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label={(
                        <span>
                        Nickname&nbsp;
                            <Tooltip title="Cómo otros usuarios te identificarán">
                                <Icon type="question-circle-o"/>
                            </Tooltip>
                        </span>
                    )}
                >
                    {getFieldDecorator('username', {
                        rules: [{required: true, message: 'Por favor introduzca su nickname', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="Nombre"
                >
                    {getFieldDecorator('nombre', {
                        rules: [{required: true, message: 'Por favor introduzca su nombre', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Register</Button>
                </FormItem>
            </Form>
        );
    }
}

export default RegistrationForm