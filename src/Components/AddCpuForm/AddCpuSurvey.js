import React from 'react';
import {Form, Input, Button, InputNumber} from 'antd';
import {message} from "antd/lib/index";
import axios from "axios/index";

const FormItem = Form.Item;
const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};
class AddCpuSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/cpu',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Modelo"
                >
                    {getFieldDecorator('model', {
                        rules: [{required: true, message: 'Por favor introduzca el modelo del CPU', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Socket"
                >
                    {getFieldDecorator('socket', {
                        rules: [{required: true, message: 'Por favor introduzca el socket del CPU', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Frecuencia de operación"
                >
                    {getFieldDecorator('operating_frequency', { rules: [{required: true, message: 'Por favor introduzca la frecuencia de operación'}] })(
                        <InputNumber min={1} max={6} step={0.1} />
                    )}
                    <span className="ant-form-text"> GHz </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Frecuencia máxima de operación"
                >
                    {getFieldDecorator('na_max_frequency', { rules: [{required: true, message: 'Por favor introduzca la frecuencia máxima de operación'}] })(
                        <InputNumber min={1} max={6} step={0.1} />
                    )}
                    <span className="ant-form-text"> GHz </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de núcleos"
                >
                    {getFieldDecorator('na_cores', { rules: [{required: true, message: 'Por favor introduzca el número de nucleos'}] })(
                        <InputNumber min={2} max={50} />
                    )}
                    <span className="ant-form-text"> núcleos </span>
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddCpuSurvey