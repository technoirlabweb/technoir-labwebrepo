import React from 'react';
import {Form, Input, Button, InputNumber, Radio} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};
class AddRamSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/ram',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de parte"
                >
                    {getFieldDecorator('part', {
                        rules: [{required: true, message: 'Por favor introduzca el número de parte', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Tipo"
                >
                    {getFieldDecorator('type', { rules: [{required: true, message: 'Por favor introduzca el tipo de memoria'}] })(
                        <RadioGroup>
                            <Radio value="DDR2">DDR2</Radio>
                            <Radio value="DDR3">DDR3</Radio>
                            <Radio value="DDR4">DDR4</Radio>
                        </RadioGroup>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Velocidad"
                >
                    {getFieldDecorator('speed', { rules: [{required: true, message: 'Por favor introduzca la velocidad del módulo'}] })(
                        <InputNumber min={1} max={7000} />
                    )}
                    <span className="ant-form-text"> MHz </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Tamaño"
                >
                    {getFieldDecorator('size', { rules: [{required: true, message: 'Por favor introduzca el tamaño de la memoria'}] })(
                        <InputNumber min={2} max={1000} />
                    )}
                    <span className="ant-form-text"> Gigabytes </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Latencia CAS"
                >
                    {getFieldDecorator('CAS_Latency', { rules: [{required: true, message: 'Por favor introduzca la latencia CAS'}] })(
                        <InputNumber min={1} max={15} />
                    )}
                    <span className="ant-form-text"> Lat </span>
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddRamSurvey