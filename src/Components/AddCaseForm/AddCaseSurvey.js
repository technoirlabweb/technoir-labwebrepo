import React from 'react';
import {Form, Input, Button, Radio, message} from 'antd';
import axios from 'axios';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};

class AddCaseSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/case',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };


    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
                <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                    <FormItem
                        {...formItemLayout}
                        label="Fabricante"
                    >
                        {getFieldDecorator('manufacturer', {
                            rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                        })(
                            <Input/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="Modelo"
                    >
                        {getFieldDecorator('model', {
                            rules: [{required: true, message: 'Por favor introduzca el modelo', whitespace: true}],
                        })(
                            <Input/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="Número de parte"
                    >
                        {getFieldDecorator('part', {
                            rules: [{
                                required: true,
                                message: 'Por favor introduzca el número de parte',
                                whitespace: true
                            }],
                        })(
                            <Input/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="Color"
                    >
                        {getFieldDecorator('color', {
                            rules: [{required: true, message: 'Por favor introduzca el color', whitespace: true}],
                        })(
                            <Input/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="Tipo"
                    >
                        {getFieldDecorator('type', {
                            rules: [{
                                required: true,
                                message: 'Por favor introduzca el tipo de gabinete'
                            }]
                        })(
                            <RadioGroup>
                                <Radio value="E-ATX">E-ATX</Radio>
                                <Radio value="ATX">ATX</Radio>
                                <Radio value="M-ATX">M-ATX</Radio>
                                <Radio value="Mini-ATX">Mini-ATX</Radio>
                            </RadioGroup>
                        )}
                    </FormItem>
                    <FormItem className="submitButton" {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Enviar</Button>
                    </FormItem>
                </Form>
        );
    }
}

export default AddCaseSurvey