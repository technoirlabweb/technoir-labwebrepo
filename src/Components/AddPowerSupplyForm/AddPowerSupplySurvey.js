import React from 'react';
import {Form, Input, Button, Radio, InputNumber, Select} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};
class AddPowerSupplySurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/power_supply',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Modelo"
                >
                    {getFieldDecorator('model', {
                        rules: [{required: true, message: 'Por favor introduzca el modelo', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de parte"
                >
                    {getFieldDecorator('part', {
                        rules: [{required: true, message: 'Por favor introduzca el número de parte', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Tipo"
                >
                    {getFieldDecorator('type', { rules: [{required: true, message: 'Por favor introduzca el tipo de fuente de poder'}] })(
                        <RadioGroup>
                            <Radio value="E-ATX">E-ATX</Radio>
                            <Radio value="ATX">ATX</Radio>
                            <Radio value="M-ATX">M-ATX</Radio>
                            <Radio value="Mini-ATX">Mini-ATX</Radio>
                        </RadioGroup>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Potencia"
                >
                    {getFieldDecorator('wattage', { rules: [{required: true, message: 'Por favor introduzca la potencia'}] })(
                        <InputNumber min={1} max={5000} step={50} />
                    )}
                    <span className="ant-form-text"> Watts </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Modularidad"
                >
                    {getFieldDecorator('modularity', { rules: [{required: true, message: 'Por favor introduzca el tipo de modularidad'}] })(
                        <RadioGroup>
                            <Radio value="Full">Full</Radio>
                            <Radio value="Semi">Semi</Radio>
                            <Radio value="Fixed">Fixed</Radio>

                        </RadioGroup>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Certificacion"
                    hasFeedback
                >
                    {getFieldDecorator('efficiency_certification', {
                        rules: [
                            { required: true, message: 'Por favor introduzca el tipo de certificacion' },
                        ],
                    })(
                        <Select placeholder="Certification">
                            <Option value="80_Plus">80 Plus </Option>
                            <Option value="80_Plus_Bronze">80 Plus Bronze</Option>
                            <Option value="80_Plus_Silver">80 Plus Silver</Option>
                            <Option value="80_Plus_Gold">80 Plus Gold</Option>
                            <Option value="80_Plus_Platinum">80 Plus Platinum</Option>
                            <Option value="80_Plus_Titanium">80 Plus Titanium</Option>
                        </Select>
                    )}
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>

            </Form>
        );
    }
}

export default AddPowerSupplySurvey