import React from 'react'
import {Card} from 'antd';
import {Link} from 'react-router-dom'

const ComputerCard = ({image, title, description, link}) =>

    <Card className="computerCard"
        title={title}
        extra={<Link to={link}>Más información</Link>}
    >
        Inner Card content
    </Card>


export default ComputerCard
