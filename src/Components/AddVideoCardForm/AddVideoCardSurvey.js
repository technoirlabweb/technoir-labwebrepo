import React from 'react';
import {Form, Input, Button, InputNumber, Switch} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;

const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};

class AddVideoCardSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/videocard',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Chipset"
                >
                    {getFieldDecorator('chipset', {
                        rules: [{required: true, message: 'Por favor introduzca el chipset de la tarjeta', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Tamaño de memoria"
                >
                    {getFieldDecorator('nu_memorySize', { rules: [{required: true, message: 'Por favor introduzca el tamaño de memoria'}] })(
                        <InputNumber min={1} max={50} />
                    )}
                    <span className="ant-form-text"> Gigabytes </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Core clock"
                >
                    {getFieldDecorator('core_clock', { rules: [{required: true, message: 'Por favor introduzca el core clock'}] })(
                        <InputNumber min={1} max={50000} step={1000} />
                    )}
                    <span className="ant-form-text"> MHz </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Boost clock"
                >
                    {getFieldDecorator('boost_clock', { rules: [{required: true, message: 'Por favor introduzca el boost clock'}] })(
                        <InputNumber min={1} max={50000} step={1000} />
                    )}
                    <span className="ant-form-text"> MHz </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Soporte de SLI"
                >
                    {getFieldDecorator('vsli', { rules: [{required: true, message: 'Por favor introduzca si la tarjeta soporta SLI'}] })(
                        <Switch />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Soporte de CrossFire"
                >
                    {getFieldDecorator('vcross', { rules: [{required: true, message: 'Por favor introduzca si la tarjeta soporta CrossFire'}] })(
                        <Switch />
                    )}
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddVideoCardSurvey
