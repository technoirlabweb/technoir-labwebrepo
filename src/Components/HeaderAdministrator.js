import React, {Component} from 'react';
import {Layout, Menu, Avatar} from 'antd';
import technoirLogo from '../Media/technoirLogo.jpg';
import {Link} from 'react-router-dom'


const {Header} = Layout;


class HeaderAdministrator extends Component {

    render() {
        return (
                    <Header>
                        <div>
                            <Menu
                                theme="dark"
                                mode="horizontal"
                                style={{lineHeight: '64px'}}
                            >
                                <Menu.Item key="1">
                                    <Link to="/"><Avatar src={technoirLogo}/></Link>
                                </Menu.Item>
                                <Menu.Item key="2"><Link to="/Registro">Registro</Link></Menu.Item>
                                <Menu.Item key="3"><Link to="/PartLists">Lista de piezas</Link></Menu.Item>
                                <Menu.Item key="4"><Link to="/AddPart">Agregar una pieza</Link></Menu.Item>
                                <Menu.Item key="5"><Link to="/UserAdministration">Administrar usuarios</Link></Menu.Item>
                                <Menu.Item key="6"><Link to="/AddPart"><div className="logoutButton">Logout</div></Link></Menu.Item>
                            </Menu>
                        </div>
                    </Header>
        );

    }
}

export default HeaderAdministrator