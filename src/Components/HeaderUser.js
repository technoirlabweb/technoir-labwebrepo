import React, {Component} from 'react';
import {Layout, Menu, Avatar} from 'antd';
import technoirLogo from '../Media/technoirLogo.jpg';
import {Link} from 'react-router-dom'


const {Header} = Layout;


class HeaderAdministrator extends Component {

    render() {
        return (
            <Header>
                <div>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{lineHeight: '64px'}}
                    >
                        <Menu.Item key="1">
                            <Avatar src={technoirLogo}/>
                        </Menu.Item>

                        <Menu.Item key="3"><Link to="PartLists/RamList">Lista de piezas</Link></Menu.Item>
                    </Menu>
                </div>
            </Header>
        );

    }
}

export default HeaderAdministrator