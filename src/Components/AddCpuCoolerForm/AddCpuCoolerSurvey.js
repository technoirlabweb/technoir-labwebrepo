import React from 'react';
import {Form, Input, Button, InputNumber, Switch} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;


const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};
class AddCpuCoolerSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/cpu_cooler',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Modelo"
                >
                    {getFieldDecorator('model', {
                        rules: [{required: true, message: 'Por favor introduzca el modelo del refrigerador', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de parte"
                >
                    {getFieldDecorator('part', {
                        rules: [{required: true, message: 'Por favor introduzca el número de parte', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Refrigerador líquido"
                >
                    {getFieldDecorator('lcv', { rules: [{required: true, message: 'Por favor introduzca si el refrigerador es de agua'}] })(
                        <Switch />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Nivel de ruido"
                >
                    {getFieldDecorator('noise_level', { rules: [{required: true, message: 'Por favor introduzca el nivel de ruido del refrigerador'}] })(
                        <InputNumber min={1} max={100} />
                    )}
                    <span className="ant-form-text"> Db </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="RPM's del refrigerador"
                >
                    {getFieldDecorator('fan_rpm', { rules: [{required: true, message: 'Por favor introduzca los RPM del refrigerador'}] })(
                        <InputNumber min={1} max={10000} />
                    )}
                    <span className="ant-form-text"> RPM </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Altura del refrigerador"
                >
                    {getFieldDecorator('height', { rules: [{required: true, message: 'Por favor introduzca la altura del refrigerador'}] })(
                        <InputNumber min={1} max={400} />
                    )}
                    <span className="ant-form-text"> mm </span>
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddCpuCoolerSurvey