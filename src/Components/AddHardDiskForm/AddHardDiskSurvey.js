import React from 'react';
import {Form, Input, Button, Radio, InputNumber, Switch} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};

class AddHardDiskSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/hard_disk',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de parte"
                >
                    {getFieldDecorator('part', {
                        rules: [{required: true, message: 'Por favor introduzca el número de parte', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Capacidad"
                >
                    {getFieldDecorator('boost_clock', {
                        rules: [{
                            required: true,
                            message: 'Por favor introduzca la capacidad'
                        }]
                    })(
                        <InputNumber min={1} max={10000} step={1000}/>
                    )}
                    <span className="ant-form-text"> Gigabytes </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Factor de forma"
                >
                    {getFieldDecorator('form_factor', {
                        rules: [{
                            required: true,
                            message: 'Por favor introduzca el factor de forma'
                        }]
                    })(
                        <RadioGroup>
                            <Radio value="2.5">2.5</Radio>
                            <Radio value="3.5">3.5</Radio>
                        </RadioGroup>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="¿Estado sólido?"
                >
                    {getFieldDecorator('ss', {
                        rules: [{
                            required: true,
                            message: 'Por favor introduzca si el disco es de estado sólido'
                        }]
                    })(
                        <Switch/>
                    )}
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddHardDiskSurvey