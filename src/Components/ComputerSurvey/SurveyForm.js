import React from 'react';
import {Form, Slider, Button, InputNumber} from 'antd';

const FormItem = Form.Item;

class SurveyForm extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 5},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="surveyForm" onSubmit={this.handleSubmit}>

                <div>
                    Dentro de una escala del 0 al 100, ¿qué tanto planea utilizar el equipo para videojuegos?
                </div>

                <FormItem
                    className="question"
                    {...formItemLayout}
                    label="Slider"
                >
                    {getFieldDecorator('slider-videojuegos')(
                        <Slider marks={{
                            0: 'Nada',
                            20: 'Muy poco',
                            40: 'Poco',
                            60: 'Regularmente',
                            80: 'Mucho',
                            100: 'Demasiado'
                        }}/>
                    )}
                </FormItem>

                <div>
                    Dentro de una escala del 0 al 100, ¿qué tanto planea utilizar el equipo para edición de video?
                </div>

                <FormItem
                    className="question"
                    {...formItemLayout}
                    label="Slider"
                >
                    {getFieldDecorator('slider-video')(
                        <Slider marks={{
                            0: 'Nada',
                            20: 'Muy poco',
                            40: 'Poco',
                            60: 'Regularmente',
                            80: 'Mucho',
                            100: 'Demasiado'
                        }}/>
                    )}
                </FormItem>

                <div>
                    Dentro de una escala del 0 al 100, ¿qué tanto planea utilizar el equipo para aplicaciones de
                    oficina?
                </div>

                <FormItem
                    className="question"
                    {...formItemLayout}
                    label="Slider"
                >
                    {getFieldDecorator('slider-oficina')(
                        <Slider marks={{
                            0: 'Nada',
                            20: 'Muy poco',
                            40: 'Poco',
                            60: 'Regularmente',
                            80: 'Mucho',
                            100: 'Demasiado'
                        }}/>
                    )}
                </FormItem>

                <div>
                    Dentro de una escala del 0 al 100, ¿qué tanto planea utilizar el equipo para rendering?
                </div>

                <FormItem
                    className="question"
                    {...formItemLayout}
                    label="Slider"
                >
                    {getFieldDecorator('slider-rendering')(
                        <Slider marks={{
                            0: 'Nada',
                            20: 'Muy poco',
                            40: 'Poco',
                            60: 'Regularmente',
                            80: 'Mucho',
                            100: 'Demasiado'
                        }}/>
                    )}
                </FormItem>

                <div className="questionText">
                    ¿Cuánto dinero tiene pensado gastar en el equipo?
                </div>

                <FormItem
                    {...formItemLayout}
                    label="Costo"
                >
                    {getFieldDecorator('price', {initialValue: 0})(
                        <InputNumber min={0}/>
                    )}
                    <span className="ant-form-text"> pesos </span>
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>

            </Form>
        );
    }
}

export default SurveyForm