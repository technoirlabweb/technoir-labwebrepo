import React from 'react';
import {Form, Input, Button, Radio, InputNumber, Switch} from 'antd';
import axios from "axios/index";
import {message} from "antd/lib/index";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

const successMessage = (messageText) => {
    message.success(messageText);
};

const errorMessage = (messageText) => {
    message.error(messageText);
};

class AddMotherboardSurvey extends React.Component {
    state = {
        confirmDirty: false,

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                axios({
                    method: 'post',
                    url: 'http://localhost:8080/v1/motherboard',
                    data: values
                }).then(function (response) {
                    successMessage("Pieza guardada con éxito");
                })
                    .catch(function (error) {
                        errorMessage("Error al guardar la pieza");
                    });
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }


    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 4,
                },
            },
        };

        return (
            <Form className="registerFormCpu" onSubmit={this.handleSubmit}>

                <FormItem
                    {...formItemLayout}
                    label="Fabricante"
                >
                    {getFieldDecorator('manufacturer', {
                        rules: [{required: true, message: 'Por favor introduzca el fabricante', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Número de parte"
                >
                    {getFieldDecorator('part', {
                        rules: [{required: true, message: 'Por favor introduzca el número de parte', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Factor de forma"
                >
                    {getFieldDecorator('form_factor', { rules: [{required: true, message: 'Por favor introduzca el factor de forma'}] })(
                        <RadioGroup>
                            <Radio value="E-ATX">E-ATX</Radio>
                            <Radio value="ATX">ATX</Radio>
                            <Radio value="M-ATX">M-ATX</Radio>
                            <Radio value="Mini-ATX">Mini-ATX</Radio>
                        </RadioGroup>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Socket del CPU"
                >
                    {getFieldDecorator('cpu_socket', {
                        rules: [{required: true, message: 'Por favor introduzca el socket del CPU', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Chipset"
                >
                    {getFieldDecorator('chipset', {
                        rules: [{required: true, message: 'Por favor introduzca el chipset', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Slots de memoria"
                >
                    {getFieldDecorator('memory_slots', { rules: [{required: true, message: 'Por favor introduzca los slots de memoria'}] })(
                        <InputNumber min={1} max={8} />
                    )}
                    <span className="ant-form-text"> slots </span>
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Soporte de SLI"
                >
                    {getFieldDecorator('vsli', { rules: [{required: true, message: 'Por favor introduzca si la tarjeta soporta SLI'}] })(
                        <Switch />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Soporte de CrossFire"
                >
                    {getFieldDecorator('vcross', { rules: [{required: true, message: 'Por favor introduzca si la tarjeta soporta CrossFire'}] })(
                        <Switch />
                    )}
                </FormItem>

                <FormItem className="submitButton" {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">Enviar</Button>
                </FormItem>
            </Form>
        );
    }
}

export default AddMotherboardSurvey