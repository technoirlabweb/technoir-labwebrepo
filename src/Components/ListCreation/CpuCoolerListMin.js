import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Nivel de ruido',
    dataIndex: 'noise_level',
    key: 'noise_level',
}, {
    title: 'Fan RPM',
    dataIndex: 'fan_rpm',
    key: 'fan_rpm',
}, {
    title: 'Refrigerador líquido',
    dataIndex: 'liquid_cooled',
    key: 'liquid_cooled',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Seleccionar</a>
    </span>
    ),
}];

class CpuCoolerList extends Component {

    constructor(props) {
        super(props);
        this.state = {cpuCoolers: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/cpu_cooler/all")
            .then(res => {
                this.setState({cpuCoolers: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table rowKey={record => record.id} className="listTable" columns={columns}
                   dataSource={this.state.cpuCoolers}/>
        );
    }
}

export default CpuCoolerList