import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Chipset',
    dataIndex: 'chipset',
    key: 'chipset',
}, {
    title: 'Tamaño de memoria',
    dataIndex: 'memory_Size',
    key: 'memory_Size',
}, {
    title: 'Core Clock',
    dataIndex: 'core_clock',
    key: 'core_clock',
}, {
    title: 'Boost Clock',
    dataIndex: 'boost_clock',
    key: 'boost_clock',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Seleccionar</a>
    </span>
    ),
}];

class VideoCardListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {videoCards: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/videocard/all")
            .then(res => {
                this.setState({videoCards: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table rowKey={record => record.id} className="listTable" columns={columns}
                   dataSource={this.state.videoCards}/>
        );
    }
}

export default VideoCardListMin