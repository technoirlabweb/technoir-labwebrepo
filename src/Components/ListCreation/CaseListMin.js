import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Número de parte',
    dataIndex: 'part',
    key: 'part',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Color',
    dataIndex: 'color',
    key: 'color',
}, {
    title: 'Tipo',
    dataIndex: 'type',
    key: 'type',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Seleccionar</a>
    </span>
    ),
}];

class CaseListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {cases: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/case/all")
            .then(res => {
                this.setState({cases: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        return (
            <Table className="listTable" rowKey={record => record.id} columns={columns}
                   dataSource={this.state.cases}/>
        );
    }
}

export default CaseListMin