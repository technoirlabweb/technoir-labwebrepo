import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';


class RamListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rams: [], columns: [{

                title: 'Fabricante',
                dataIndex: 'manufacturer',
                key: 'manufacturer',
            }, {
                title: 'Número de parte',
                dataIndex: 'part',
                key: 'part',
            }, {
                title: 'Tipo',
                dataIndex: 'type',
                key: 'type',
            }, {
                title: 'Velocidad',
                dataIndex: 'speed',
                key: 'speed',
            }, {
                title: 'Tamaño',
                dataIndex: 'size',
                key: 'size',
            }, {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
      <a onClick={() => this.props.addRam(record)}>Seleccionar</a>
    </span>
                ),
            }]
        };
    }


    componentWillMount() {
        axios.get("http://localhost:8080/v1/ram/all")
            .then(res => {
                this.setState({rams: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        return (
            <Table rowKey={record => record.id} className="listTable" columns={this.state.columns}
                   dataSource={this.state.rams}/>
        );
    }
}

export default RamListMin