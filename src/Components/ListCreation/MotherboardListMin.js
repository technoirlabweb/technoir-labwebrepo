import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

class MotherboardListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {motherboards: [], columns: [{

                title: 'Fabricante',
                dataIndex: 'manufacturer',
                key: 'manufacturer',
            }, {
                title: 'Número de parte',
                dataIndex: 'part',
                key: 'part',
            }, {
                title: 'Factor de forma',
                dataIndex: 'form_factor',
                key: 'form_factor',
            }, {
                title: 'Socket CPU',
                dataIndex: 'cpu_socket',
                key: 'cpu_socket',
            }, {
                title: 'Chipset',
                dataIndex: 'chipset',
                key: 'chipset',
            }, {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
            <a onClick={() => this.props.addMotherboard(record)}>Seleccionar</a>
    </span>
                ),
            }]};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/motherboard/all")
            .then(res => {
                this.setState({motherboards: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table rowKey={record => record.id} className="listTable" columns={this.state.columns}
                   dataSource={this.state.motherboards}/>
        );
    }
}

export default MotherboardListMin