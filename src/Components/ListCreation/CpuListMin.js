import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';




class CpuListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {cpus: [], columns: [{

                title: 'Fabricante',
                dataIndex: 'manufacturer',
                key: 'manufacturer',
            }, {
                title: 'Modelo',
                dataIndex: 'model',
                key: 'model',
            }, {
                title: 'Socket',
                dataIndex: 'socket',
                key: 'socket',
            }, {
                title: 'Frecuencia de operación (GHz)',
                dataIndex: 'operating_frequency',
                key: 'operating_frequency',
            }, {
                title: 'Frecuencia máxima de operación (GHz)',
                dataIndex: 'na_max_frequency',
                key: 'na_max_frequency',
            }, {
                title: 'Número de núcleos',
                dataIndex: 'na_cores',
                key: 'na_cores',
            }, {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
      <a onClick={() => this.props.addCpu(record)}>Seleccionar</a>
    </span>
                ),
            }]};


    }



    componentWillMount() {
        axios.get("http://localhost:8080/v1/cpu/all")
            .then(res => {
                this.setState({cpus: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table rowKey={record => record.id} className="listTableMin" columns={this.state.columns}
                   dataSource={this.state.cpus}/>
        );
    }
}

export default CpuListMin