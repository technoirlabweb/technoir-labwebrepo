import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Número de parte',
    dataIndex: 'part',
    key: 'part',
}, {
    title: 'Capacidad',
    dataIndex: 'capacity',
    key: 'capacity',
}, {
    title: 'Factor de forma',
    dataIndex: 'form_factor',
    key: 'form_factor',
}, {
    title: 'Estado sólido',
    dataIndex: 'solid_state',
    key: 'solid_state',
    render: text => <div>{text === 1 ? 'Sí' : 'No'}</div>,
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Seleccionar</a>
    </span>
    ),
}];

class HardDiskListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {hardDisks: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/hard_disk/all")
            .then(res => {
                this.setState({hardDisks: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table rowKey={record => record.id} className="listTable" columns={columns}
                   dataSource={this.state.hardDisks}/>
        );
    }
}

export default HardDiskListMin