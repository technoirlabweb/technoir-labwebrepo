import React, {Component} from 'react';
import {Table} from 'antd';
import axios from 'axios';

const columns = [{

    title: 'Fabricante',
    dataIndex: 'manufacturer',
    key: 'manufacturer',
}, {
    title: 'Modelo',
    dataIndex: 'model',
    key: 'model',
}, {
    title: 'Tipo',
    dataIndex: 'type',
    key: 'type',
}, {
    title: 'Watts',
    dataIndex: 'wattage',
    key: 'wattage',
}, {
    title: 'Modularidad',
    dataIndex: 'modularity',
    key: 'modularity',
}, {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
        <span>
      <a href="">Seleccionar</a>
    </span>
    ),
}];

class PowerSupplyListMin extends Component {

    constructor(props) {
        super(props);
        this.state = {powerSupplies: []};
    }

    componentWillMount() {
        axios.get("http://localhost:8080/v1/power_supply/all")
            .then(res => {
                this.setState({powerSupplies: res.data});
            }).catch(function (error) {
            console.log(error);
        });
    }


    render() {
        return (
            <Table className="listTable" rowKey={record => record.id} columns={columns}
                   dataSource={this.state.powerSupplies}/>
        );
    }
}

export default PowerSupplyListMin